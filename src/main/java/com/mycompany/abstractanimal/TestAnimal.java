/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abstractanimal;

/**
 *
 * @author ACER
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Karn");
        Cat cat1 = new Cat("Rambo");
        Dog dog1 = new Dog("BoBa");
        Snake snake1 = new Snake("Font");
        Crocodile croc1 = new Crocodile("Guapo");
        Crab crab1 = new Crab("Dodo");
        Fish fish1 = new Fish("Redd");
        Bird bird1 = new Bird("JonJon");
        Bat bat1 = new Bat("Batzy");
        h1.eat();
        h1.walk();
        h1.run();
        Animal a1 = h1;
        System.out.println("h1 is animal? "+ (h1 instanceof Animal));
        System.out.println("h1 is land animal? "+ (h1 instanceof LandAnimal));
        System.out.println("a1 is animal? "+ (a1 instanceof Animal));
        System.out.println("a1 is reptile animal? "+ (a1 instanceof Reptile));
        System.out.println("-----------------");
        cat1.run();
        cat1.sleep();
        cat1.speak();
        Animal c1 = cat1;
        System.out.println("cat1 is animal? "+ (cat1 instanceof Animal));
        System.out.println("cat1 is land animal? "+ (cat1 instanceof LandAnimal));
        System.out.println("c1 is animal? "+ (c1 instanceof Animal));
        System.out.println("c1 is poultry animal? "+ (c1 instanceof Poultry));
        System.out.println("c1 is aquatic animal? "+ (c1 instanceof AquaticAnimal));
        System.out.println("-----------------");
        dog1.walk();
        dog1.speak();
        dog1.run();
        dog1.eat();
        Animal d1 = dog1;
        System.out.println("dog1 is animal? "+ (dog1 instanceof Animal));
        System.out.println("dog1 is land animal? "+ (dog1 instanceof LandAnimal));
        System.out.println("d1 is animal? "+ (d1 instanceof Animal));
        System.out.println("d1 is reptile animal? "+ (d1 instanceof Reptile));
        System.out.println("d1 is poultry animal? "+ (d1 instanceof Poultry));
        System.out.println("d1 is aquatic animal? "+ (d1 instanceof AquaticAnimal));
        System.out.println("-----------------");
        snake1.crawl();
        snake1.sleep();
        snake1.eat();
        snake1.speak();
        Animal s1 = snake1;
        System.out.println("snake1 is animal? "+ (snake1 instanceof Animal));
        System.out.println("snake1 is reptile animal? "+ (snake1 instanceof Reptile));
        System.out.println("s1 is animal? "+ (s1 instanceof Animal));
        System.out.println("s1 is reptile animal? "+ (s1 instanceof Reptile));
        System.out.println("s1 is land animal? "+ (s1 instanceof LandAnimal));
        System.out.println("s1 is aquatic animal? "+ (s1 instanceof AquaticAnimal));
        System.out.println("-----------------");
        croc1.crawl();
        croc1.eat();
        croc1.walk();
        croc1.sleep();
        Animal c2 = croc1;
        System.out.println("croc1 is animal? "+ (croc1 instanceof Animal));
        System.out.println("croc1 is reptile animal? "+ (croc1 instanceof Reptile));
        System.out.println("c2 is animal? "+ (c2 instanceof Animal));
        System.out.println("c2 is poultry animal? "+ (c2 instanceof Poultry));
        System.out.println("c2 is land animal? "+ (c2 instanceof LandAnimal));
        System.out.println("c2 is aquatic animal? "+ (c2 instanceof AquaticAnimal));
        System.out.println("-----------------");
        crab1.swim();
        crab1.walk();
        crab1.sleep();
        Animal c3 = crab1;
        System.out.println("crab1 is animal? "+ (crab1 instanceof Animal));
        System.out.println("crab1 is aquatic animal? "+ (crab1 instanceof AquaticAnimal));
        System.out.println("c3 is animal? "+ (c3 instanceof Animal));
        System.out.println("c3 is poultry animal? "+ (c3 instanceof Poultry));
        System.out.println("c3 is land animal? "+ (c3 instanceof LandAnimal));
        System.out.println("c3 is reptile animal? "+ (c3 instanceof Reptile));
        System.out.println("-----------------");
        fish1.swim();
        fish1.eat();
        fish1.sleep();
        Animal f1 = fish1;
        System.out.println("fish1 is animal? "+ (fish1 instanceof Animal));
        System.out.println("fish1 is aquatic animal? "+ (fish1 instanceof AquaticAnimal));
        System.out.println("f1 is animal? "+ (f1 instanceof Animal));
        System.out.println("f1 is poultry animal? "+ (f1 instanceof Poultry));
        System.out.println("f1 is land animal? "+ (f1 instanceof LandAnimal));
        System.out.println("f1 is reptile animal? "+ (f1 instanceof Reptile));
        System.out.println("-----------------");
        bird1.eat();
        bird1.walk();
        bird1.speak();
        bird1.fly();
        bird1.sleep();
        Animal b1 = bird1;
        System.out.println("bird1 is animal? "+ (bird1 instanceof Animal));
        System.out.println("bird1 is poultry animal? "+ (bird1 instanceof Poultry));
        System.out.println("b1 is animal? "+ (b1 instanceof Animal));
        System.out.println("b1 is poultry animal? "+ (b1 instanceof LandAnimal));
        System.out.println("b1 is land animal? "+ (b1 instanceof AquaticAnimal));
        System.out.println("b1 is reptile animal? "+ (b1 instanceof Reptile));
        System.out.println("-----------------");
        bat1.fly();
        bat1.speak();
        bat1.walk();
        bat1.eat();
        bat1.sleep();
        Animal b2 = bat1;
        System.out.println("bat1 is animal? "+ (bat1 instanceof Animal));
        System.out.println("bat1 is poultry animal? "+ (bat1 instanceof Poultry));
        System.out.println("b2 is animal? "+ (b2 instanceof Animal));
        System.out.println("b2 is poultry animal? "+ (b2 instanceof LandAnimal));
        System.out.println("b2 is reptile animal? "+ (b2 instanceof Reptile));
    }
}
