/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.abstractanimal;

/**
 *
 * @author ACER
 */
public class TestInterface {
    public static void main(String[] args) {
        Bat bat = new Bat("Batzy");
        Plane plane = new Plane("Engine Number 01");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("DeeDee");
        Crocodile croc = new Crocodile("Dang");
        croc.crawl();
        Snake snake = new Snake("Font");
        snake.crawl();
        Crab crab = new Crab("DoDo");
        Fish fish = new Fish("Redd");
        fish.swim();
        crab.eat();
        Cat cat = new Cat("Ponzu");
        cat.run();
        Bird bird = new Bird("JonJon");
        Human human = new Human("Karn");
        System.out.println("----------");
        
        Flyable[] flyables = {bat,plane,bird};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        System.out.println("-----------");
        
        Runable[] runables = {dog,plane,cat,human};
        for(Runable r : runables){
            r.run();
        }
        System.out.println("-----------");
        
        Crawlable[] crawlables = {croc,snake};
        for(Crawlable c : crawlables){
            c.crawl();
        }
        System.out.println("-----------");
        Swimable[] swimables = {crab,fish};
        for(Swimable s : swimables){
            s.swim();
        }
        System.out.println("-----------");
    }
}
